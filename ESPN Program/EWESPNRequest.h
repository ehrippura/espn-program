//
//  EWESPNRequest.h
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/21.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EWESPNRequestDelegate;

@interface EWESPNRequest : NSObject {
    NSDate *requestDate;
    id <EWESPNRequestDelegate> delegate;
    NSMutableData *data;
    NSURLConnection *connection;
    NSMutableArray *responser;
}

- (id)initWithRequestDate:(NSDate *)date delegate:(id <EWESPNRequestDelegate>)delegate;
- (void)connect;
- (void)setDelegate:(id <EWESPNRequestDelegate>) delegate;
- (NSArray *)response;

@end


@protocol EWESPNRequestDelegate <NSObject>
@optional
- (void)requestWillStartLoading:(EWESPNRequest *)request;
- (void)requestDidFinishedLoading:(EWESPNRequest *)request;
- (void)request:(EWESPNRequest *)request didFailedWithError:(NSError *)error;

@end
