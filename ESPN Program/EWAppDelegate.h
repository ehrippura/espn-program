//
//  EWAppDelegate.h
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/20.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface EWAppDelegate : NSObject {
    IBOutlet NSWindow *window;
}

- (NSWindow *)window;

@end
