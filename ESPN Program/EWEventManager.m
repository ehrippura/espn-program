//
//  EWEventManager.m
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/8/14.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWEventManager.h"

NSString *const EWESPNProgramCalendarName = @"ESPN Program Event";
NSString *const EWESPNPrefProgramCalendarUID = @"tw.eternalwind.calendar.uid";

@implementation EWEventManager

@synthesize alerted;

+ (EWEventManager *)sharedManager
{
    static id master = nil;
    @synchronized (self) {
        if (!master)
            master = [self new];
    }
    return master;
}

+ (void)initialize
{
    // create espn calendar calendar if not exist
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *uid = [pref objectForKey:EWESPNPrefProgramCalendarUID];
    
    if (!uid) {
        CalCalendarStore *store = [CalCalendarStore defaultCalendarStore];
        CalCalendar *calendar = nil;
        for (CalCalendar *cal in [store calendars]) {
            if ([cal.title isEqualToString:EWESPNProgramCalendarName]) {
                calendar = cal;
                break;
            }
        }
        
        if (!calendar) {
            calendar = [CalCalendar calendar];
            calendar.title = EWESPNProgramCalendarName;
        }
        
        NSError *err = nil;
        if (![store saveCalendar:calendar error:&err]) {
            NSAlert *alert = [NSAlert alertWithError:err];
            [alert runModal];
        } else {
            [pref setObject:calendar.uid forKey:EWESPNPrefProgramCalendarUID];
            [pref synchronize];
        }
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        NSString *uid = [pref objectForKey:EWESPNPrefProgramCalendarUID];
        
        _calendar = [[[CalCalendarStore defaultCalendarStore] calendarWithUID:uid] retain];
    }
    return self;
}

- (void)dealloc
{
    [_calendar release];
    [super dealloc];
}

- (BOOL)addEventWithTitle:(NSString *)title startTime:(NSDate *)startTime endTime:(NSDate *)endTime
{
    CalEvent *event = [CalEvent event];
    
    event.isAllDay = NO;
    event.startDate = startTime;
    event.endDate = endTime;
    event.title = title;
    event.calendar = _calendar;
    
    CalCalendarStore *store = [CalCalendarStore defaultCalendarStore];
    NSError *err = nil;
    if (![store saveEvent:event span:CalSpanAllEvents error:&err]) {
        NSAlert *alert = [NSAlert alertWithError:err];
        [alert runModal];
    } else {
        NSAlert *alert = [NSAlert alertWithMessageText:@"Success"
                                         defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"Program %@ was added to iCal", event.title];
        [alert runModal];
    }
    
    return NO;
}

@end
