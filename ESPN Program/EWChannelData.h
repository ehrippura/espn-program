//
//  EWChannelData.h
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/21.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EWProgramData;

#define EWNumberOfChannel       3
enum {
    EWProgramChannelESPN = 0,
    EWProgramChannelSTAR,
    EWProgramChannelESPNHD,
};
typedef NSInteger EWProgramChannel;

@interface EWChannelData : NSObject {
    EWProgramChannel channel;
    NSMutableArray *programs;
}

- (id)initWithChannel:(EWProgramChannel)channelType;
- (id)initWithChannel:(EWProgramChannel)channelType programs:(EWProgramData *)firstProgram, ... NS_REQUIRES_NIL_TERMINATION;

- (void)addProgram:(EWProgramData *)programData;
- (NSArray *)programs;

@end
