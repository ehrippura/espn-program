//
//  EWProgramData.h
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/20.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSInteger EWProgramType;

@interface EWProgramData : NSObject {
    NSDate *startTime;
    NSDate *endTime;
    
    NSString *title;
    EWProgramType type;
}

+ (EWProgramData *)programWithTitle:(NSString *)title startTime:(NSDate *)sTime endTime:(NSDate *)eTime;
+ (EWProgramData *)programWithTitle:(NSString *)title startTime:(NSDate *)sTime duration:(NSDate *)duration;

- (id)initWithTitle:(NSString *)title startTime:(NSDate *)sTime endTime:(NSDate *)eTime;
- (void)setType:(EWProgramType)type;

- (NSDate *)startTime;
- (NSDate *)endTime;
- (NSString *)title;

- (NSColor *)programColor;

@end
