//
//  EWPopoverWindowController.m
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/8/16.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWPopoverWindowController.h"
#import "EWEventManager.h"

#import "SFBPopover/SFBPopoverWindow.h"
#import "SFBPopover/SFBPopoverWindowController.h"


@implementation EWPopoverWindowController

- (id)init
{
    self = [super init];
    if (self) {
        [NSBundle loadNibNamed:@"EWPopoverWindow" owner:self];
        [[self window] setPopoverPosition:SFBPopoverPositionLeft];
        [[self window] setPopoverBackgroundColor:[NSColor colorWithCalibratedRed:0.98f green:0.98f blue:0.98f alpha:1.00f]];
        
        _windowController = [[SFBPopoverWindowController alloc] initWithWindow:_window];
        
        _startTime = nil;
        _endTime = nil;
    }
    return self;
}

- (void)dealloc
{
    [_windowController release];
    [_startTime release];
    [_endTime release];
    [super dealloc];
}

- (SFBPopoverWindow *)window
{
    return _window;
}

- (SFBPopoverWindowController *)popoverWindowController
{
    return _windowController;
}

- (IBAction)add:(id)sender
{
    NSString *title = [_titleLabel stringValue];
    
    if ([[EWEventManager sharedManager] addEventWithTitle:title
                                                startTime:_startTime
                                                  endTime:_endTime]) {
        if ([_openICalBox state] == NSOnState) {
            NSAppleScript *script = [[NSAppleScript alloc] initWithContentsOfURL:nil error:nil];
            
            [script release];
        }
    }
    
    [_windowController closePopover:sender];
}

- (IBAction)cancel:(id)sender
{
    [_windowController closePopover:sender];
}

- (void)setTitle:(NSString *)title
{
    [_titleLabel setStringValue:title];
}

- (void)setStartTime:(NSDate *)startTime
{
    [_startTime release];
    _startTime = [startTime retain];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy/MM/dd HH:mm"];
    [_startTimeLabel setStringValue:[df stringFromDate:startTime]];
}

- (void)setEndTime:(NSDate *)endTime
{
    [_endTime release];
    _endTime = [endTime retain];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy/MM/dd HH:mm"];
    [_endTimeLabel setStringValue:[df stringFromDate:endTime]];
}

@end
