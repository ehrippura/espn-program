//
//  EWEventManager.h
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/8/14.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CalendarStore/CalendarStore.h>

extern NSString *const EWESPNProgramCalendarName ;
extern NSString *const EWESPNPrefProgramCalendarUID ;

@interface EWEventManager : NSObject {
    CalCalendar *_calendar;
    id _delegate;
    BOOL alerted;
}

@property (nonatomic, assign, getter = isAlert) BOOL alerted; // placeholder

+ (EWEventManager *)sharedManager;
- (BOOL)addEventWithTitle:(NSString *)title startTime:(NSDate *)startTime endTime:(NSDate *)endTime;

@end
