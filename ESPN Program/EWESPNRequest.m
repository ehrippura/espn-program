//
//  EWESPNRequest.m
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/21.
//  Copyright (c) 2012 Tzu-Yi Lin. All rights reserved.
//

#import "EWESPNRequest.h"
#import "EWChannelData.h"
#import "EWProgramData.h"


NSString *const kESPNProgramURL = @"http://www.espnstar.com.tw/program/";
const NSTimeInterval kTimeout = 180.0;

#define NSBig5StringEncoding     CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5)
#define skipObject(enum, obj)    obj = [enum nextObject]; ((void)0)

@implementation EWESPNRequest

- (id)initWithRequestDate:(NSDate *)date delegate:(id <EWESPNRequestDelegate>)_delegate;
{
    self = [super init];
    if (self) {
        connection = nil;
        delegate = _delegate;
        requestDate = (date) ? [date retain] : [[NSDate date] retain];
        data = nil;
    }
    return self;
}

- (void)dealloc
{
    if (connection) {
        [connection cancel];
        [connection release];
    }
    [requestDate release];
    [data release];
    [responser release];
    
    [super dealloc];
}

- (void)setDelegate:(id<EWESPNRequestDelegate>)_delegate
{
    delegate = _delegate;
}

- (void)connect
{
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *nowComponent = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *savedComponent = [calendar components:unitFlags fromDate:requestDate];
    [calendar release];
    
    NSURLRequest *request = nil;
    
    if ([nowComponent day] == [savedComponent day]) {
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:kESPNProgramURL]
                                   cachePolicy:NSURLRequestReloadIgnoringCacheData
                               timeoutInterval:kTimeout];
    } else {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [df stringFromDate:requestDate];
        NSString *requestURL = [NSString stringWithFormat:@"%@?rdate=%@", kESPNProgramURL, dateString];
        
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestURL]
                                   cachePolicy:NSURLRequestReloadIgnoringCacheData
                               timeoutInterval:kTimeout];
        
        [df release];
    }
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if ([delegate respondsToSelector:@selector(requestWillStartLoading:)])
        [delegate requestWillStartLoading:self];
}

- (BOOL)parsingData
{
    NSError *error = nil;
    NSString *strData = [[NSString alloc] initWithData:data encoding:NSBig5StringEncoding];
    NSXMLDocument *document = [[NSXMLDocument alloc] initWithXMLString:strData
                                                               options:NSXMLDocumentTidyHTML
                                                                 error:&error];
    [strData release];
    
    if (document) {
        // get table
        NSString *xpath = @"//table";
        NSArray *channelArray = [[document rootElement] nodesForXPath:xpath error:nil];
        
        NSAssert([channelArray count] == 3, @"Channel Number Error");
        
        [responser release];
        responser = [[NSMutableArray alloc] init];

        for (NSXMLElement *channelInfo in channelArray) {
            NSArray *td = [channelInfo nodesForXPath:@"tbody/*/td" error:nil];
            NSArray *allValue = [td valueForKeyPath:@"@unionOfObjects.stringValue"];
            
            NSEnumerator *e = [allValue objectEnumerator];
            NSString *obj;
            
            EWChannelData *channelData = [[EWChannelData alloc] initWithChannel:[channelArray indexOfObject:channelInfo]
                                                                       programs:nil];
            
            while (obj = [e nextObject]) {
                NSUInteger unit = NSMinuteCalendarUnit | NSHourCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit;
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                [calendar setTimeZone:[NSTimeZone localTimeZone]];
                NSDateComponents *com = [calendar components:unit fromDate:requestDate];
                
                NSArray *temp = [obj componentsSeparatedByString:@":"];
                
                com.hour = [[temp objectAtIndex:0] integerValue];
                com.minute = [[temp lastObject] integerValue];
                NSDate *sTime = [calendar dateFromComponents:com];
                skipObject(e, obj);
                
                NSString *title = obj;
                skipObject(e, obj);
                
                com = [[NSDateComponents alloc] init];
                temp = [obj componentsSeparatedByString:@":"];
                com.hour = [[temp objectAtIndex:0] integerValue];
                com.minute = [[temp lastObject] integerValue];
                NSDate *eTime = [calendar dateFromComponents:com];
                skipObject(e, obj);
                [com release];
                
                [calendar release];
                
                [channelData addProgram:[EWProgramData programWithTitle:title startTime:sTime duration:eTime]];
            }
            
            [responser addObject:channelData];
            [channelData release];
        }
    } else {
        if (delegate && [delegate respondsToSelector:@selector(request:didFailedWithError:)])
            [delegate request:self didFailedWithError:error];
    }
    
    BOOL retVal = (document) ? YES : NO;
    [document release];
    
    return retVal;
}

- (NSArray *)response
{
    return [[responser copy] autorelease];
}

// delegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"%@", response);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)receivedData
{
    if (!data)
        data = [[NSMutableData alloc] init ];
    
    [data appendData:receivedData];
}

- (void)connection:(NSURLConnection *)_connection didFailWithError:(NSError *)error
{
    [data release];
    data = nil;
    [connection release];
    
    if (delegate && [delegate respondsToSelector:@selector(request:didFailedWithError:)])
        [delegate request:self didFailedWithError:error];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)_connection
{
    [connection release];
    connection = nil;
    BOOL result = [self parsingData];
    
    if (result && delegate && [delegate respondsToSelector:@selector(requestDidFinishedLoading:)])
        [delegate requestDidFinishedLoading:self];
}

@end
