//
//  EWAppDelegate.m
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/20.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWAppDelegate.h"
#import "EWPopoverWindowController.h"

@implementation EWAppDelegate

- (void)dealloc
{
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    EWPopoverWindowController *con = [[EWPopoverWindowController alloc] init];
    [con release];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

- (NSWindow *)window
{
    return window;
}

@end
