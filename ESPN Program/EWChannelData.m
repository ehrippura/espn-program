//
//  EWChannelData.m
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/21.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWChannelData.h"
#import "EWProgramData.h"

@implementation EWChannelData

- (id)initWithChannel:(EWProgramChannel)channelType
{
    self = [super init];
    if (self) {
        channel = channelType;
        programs = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithChannel:(EWProgramChannel)channelType programs:(EWProgramData *)firstProgram, ...
{
    self = [self initWithChannel:channelType];
    if (self) {
        va_list ap;
        va_start(ap, firstProgram);
        while (firstProgram)
            [self addProgram:firstProgram];
        va_end(ap);
    }
    
    return self;
}

- (void)addProgram:(EWProgramData *)programData
{
    [programs addObject:programData];
}

- (NSArray *)programs
{
    return [[programs copy] autorelease];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Channel: %ld, Number of programs: %lu", channel, [programs count]];
}

@end
