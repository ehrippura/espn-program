//
//  EWProgramController.m
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/20.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWProgramController.h"
#import "EWPopoverWindowController.h"
#import "EWChannelData.h"
#import "EWProgramData.h"
#import "EWAppDelegate.h"

@class EWPopoverWindowController;

@interface EWProgramController () 

- (void)addCalendar:(NSButtonCell *)sender;
- (void)filterTable:(NSString *)searchString;

@end

@implementation EWProgramController 

- (id)init
{
    self = [super init];
    if (self) {
        request = [[EWESPNRequest alloc] initWithRequestDate:[NSDate date] delegate:self];
        [request connect];
        filtered = NO;
        filteredProgram = [[NSMutableArray alloc] init];
        confirmPopover = [[EWPopoverWindowController alloc] init];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [progressIndicator startAnimation:self];
    
    [dateButton removeAllItems];
    // set date button for week
    NSDate *date = [NSDate date]; // today
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterNoStyle];
    [df setDateStyle:NSDateFormatterMediumStyle];
    [df setLocale:[NSLocale currentLocale]];
    
    for (int i = 0; i < 8; i++) { // week
        NSDate *newDate = [date addTimeInterval:86400 * i];
        [dateButton addItemWithTitle:[df stringFromDate:newDate]];
    }
    
    // set search bar menu
    NSMenu *menu = [[NSMenu alloc] init];
    NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:@"Placeholder" action:@selector(startSearch:) keyEquivalent:@""];
    [menu addItem:item];
    [item release];
    [[searchField cell] setSearchMenuTemplate:menu];
    [menu release];
}

- (void)dealloc
{
    [channels release];
    [filteredProgram release];
    [confirmPopover release];
    
    [super dealloc];
}

- (void)filterTable:(NSString *)searchString
{
    filtered = [searchString length] != 0;
    [filteredProgram removeAllObjects];
    
    if (filtered) {
        NSArray *programs = [currentChannel programs];
        for (EWProgramData *pdata in programs) {
            if ([[pdata title] rangeOfString:searchString].location != NSNotFound) 
                [filteredProgram addObject:pdata];
        }
    }
}

// UI action
- (IBAction)channelChanged:(id)sender
{
    currentChannel = [channels objectAtIndex:[sender indexOfSelectedItem]];
    [tableView reloadData];
}

- (IBAction)dateChanged:(id)sender
{
    NSString *buttonTitle = [sender titleOfSelectedItem];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeStyle:NSDateFormatterNoStyle];
    [df setDateStyle:NSDateFormatterMediumStyle];
    [df setLocale:[NSLocale currentLocale]];
    
    NSDate *queryDate = [df dateFromString:buttonTitle];
    [request release];
    [df release];
    request = [[EWESPNRequest alloc] initWithRequestDate:queryDate delegate:self];
    [request connect];
}

- (IBAction)startSearch:(id)sender
{
    NSString *searchString = nil;
    // set history menu
    if ([sender isKindOfClass:[NSMenuItem class]]) {
        searchString = [sender title];
        [searchField setStringValue:searchString];
    } else {
        searchString = [sender stringValue];
    }
    
    // filter
    [self filterTable:searchString];
    
    NSMenu *menu = [[searchField cell] searchMenuTemplate];
    while ([[menu itemArray] count] != 0)
        [menu removeItemAtIndex:0];
    
    for (NSString *str in [searchField recentSearches]) {
        NSMenuItem *menuItem = [[NSMenuItem alloc] initWithTitle:str
                                                          action:@selector(startSearch:)
                                                   keyEquivalent:@""];
        [menuItem setTarget:self];
        [menu addItem:menuItem];
        [menuItem release];
    }
        
    [tableView reloadData];
}

- (void)addCalendar:(NSButtonCell *)sender
{
    NSInteger row = [tableView clickedRow];
    NSRect colRect = [tableView rectOfColumn:[tableView columnWithIdentifier:@"addCalendar"]];
    NSRect rowRect = [tableView rectOfRow:row];
    
    EWAppDelegate *delegate = (EWAppDelegate *)[[NSApplication sharedApplication] delegate];
    
    CGRect intersectRect = CGRectIntersection(NSRectToCGRect(colRect), NSRectToCGRect(rowRect));
    NSRect re = NSRectFromCGRect(intersectRect);
    re = [tableView convertRect:re toView:[[delegate window] contentView]];
    
    NSPoint point = NSMakePoint(re.origin.x + re.size.width / 2,
                                re.origin.y + re.size.height / 2);
    
    EWProgramData *program = [[currentChannel programs] objectAtIndex:row];
    
    [confirmPopover setTitle:[program title]];
    [confirmPopover setStartTime:[program startTime]];
    [confirmPopover setEndTime:[program endTime]];
    
    if ([[confirmPopover window] isVisible])
        [[confirmPopover popoverWindowController] movePopoverToPoint:point];
    else
        [[confirmPopover popoverWindowController] displayPopoverInWindow:[delegate window] atPoint:point];
}

// delegate
- (void)request:(EWESPNRequest *)_request didFailedWithError:(NSError *)error
{
    [progressIndicator stopAnimation:self];
}

- (void)requestDidFinishedLoading:(EWESPNRequest *)_request
{
    [channels release];
    channels = [[_request response] retain];
    [channelButton selectItemAtIndex:0];
    currentChannel = [channels objectAtIndex:[channelButton indexOfSelectedItem]];
    
    if ([[searchField stringValue] length] != 0)
        [self filterTable:[searchField stringValue]];
    
    [tableView reloadData];
    
    [progressIndicator stopAnimation:self];
}

- (void)requestWillStartLoading:(EWESPNRequest *)request
{
    [progressIndicator startAnimation:self];
}

// tableview datasource
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    NSArray *programs = (filtered) ? filteredProgram : [currentChannel programs];
    return [programs count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSArray *programs = (filtered) ? filteredProgram : [currentChannel programs];
    
    EWProgramData *programData = [programs objectAtIndex:row];
    
    if ([[tableColumn identifier] isEqualToString:@"title"])
        return [programData title];
    else if ([[tableColumn identifier] isEqualToString:@"time"]) {
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [dateFormatter setDateFormat:@"HH:mm"];
        return [NSString stringWithFormat:@"%@ - %@", [dateFormatter stringFromDate:[programData startTime]],
                                                      [dateFormatter stringFromDate:[programData endTime]]];
    }

    return nil;
}

- (NSCell *)tableView:(NSTableView *)tableView dataCellForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    if ([[tableColumn identifier] isEqualToString:@"addCalendar"]) {
        NSButtonCell *buttonCell = [[[NSButtonCell alloc] initImageCell:[NSImage imageNamed:NSImageNameAddTemplate]] autorelease];
        
        [buttonCell setTarget:self];
        [buttonCell setAction:@selector(addCalendar:)];
        
        return buttonCell;
    }

    NSCell *cell = [tableColumn dataCell];
    if ([cell isKindOfClass:[NSTextFieldCell class]]) {
        NSTextFieldCell *textCell = (NSTextFieldCell *)cell;
        EWProgramData *program = [currentChannel.programs objectAtIndex:row];
        [textCell setTextColor:[program programColor]];
    }
    return cell;
}

@end
