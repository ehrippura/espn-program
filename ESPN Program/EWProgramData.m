//
//  EWProgramData.m
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/20.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import "EWProgramData.h"


#define EWProgramColorBefore    [NSColor blackColor]
#define EWProgramColorAfter     [NSColor grayColor]

@implementation EWProgramData 

- (id)initWithTitle:(NSString *)_title startTime:(NSDate *)sTime endTime:(NSDate *)eTime
{
    self = [super init];
    if (self) {
        title = [_title copy];
        startTime = [sTime copy];
        endTime = [eTime copy];
    }
    
    return self;
}

- (void)dealloc
{
    [title release];
    [startTime release];
    [endTime release];
    [super dealloc];
}

+ (EWProgramData *)programWithTitle:(NSString *)title startTime:(NSDate *)sTime endTime:(NSDate *)eTime
{
    
    return [[[self alloc] initWithTitle:title startTime:sTime endTime:eTime] autorelease];
}

+ (EWProgramData *)programWithTitle:(NSString *)title startTime:(NSDate *)sTime duration:(NSDate *)duration
{
    NSUInteger unit = NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:unit fromDate:duration];
    
    return [[[self alloc] initWithTitle:title startTime:sTime endTime:[calendar dateByAddingComponents:components
                                                                                                toDate:sTime
                                                                                               options:0]] autorelease];
}

- (NSColor *)programColor
{
    NSDate *currentDate = [NSDate date];
    NSComparisonResult result = [currentDate compare:endTime];
    
    if (result == NSOrderedDescending) // later, is over
        return EWProgramColorAfter;
    
    return EWProgramColorBefore;
}

- (void)setType:(EWProgramType)aType
{
    type = aType;
}

- (NSDate *)startTime
{
    return startTime;
}

- (NSDate *)endTime
{
    return endTime;
}

- (NSString *)title
{
    return title;
}

- (NSString *)description
{
    NSString *str = [NSString stringWithFormat:@"<%@ - %p: Program: %@, From %@ to %@", [self class], self, title, startTime, endTime];
    
    return str;
}

@end
