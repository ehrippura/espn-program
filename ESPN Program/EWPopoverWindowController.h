//
//  EWPopoverWindowController.h
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/8/16.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFBPopover/SFBPopoverWindowController.h"

@class SFBPopoverWindow;

@interface EWPopoverWindowController : NSObject {
    SFBPopoverWindowController *_windowController;
    
    IBOutlet SFBPopoverWindow *_window;
    IBOutlet NSTextField *_titleLabel;
    IBOutlet NSTextField *_startTimeLabel;
    IBOutlet NSTextField *_endTimeLabel;
    IBOutlet NSButton    *_openICalBox;
    
    NSDate *_startTime;
    NSDate *_endTime;
}

- (SFBPopoverWindow *)window;

- (SFBPopoverWindowController *)popoverWindowController;

// action for add button
- (IBAction)add:(id)sender;

// action for cancele button
- (IBAction)cancel:(id)sender;

- (void)setTitle:(NSString *)title;
- (void)setStartTime:(NSDate *)startTime;
- (void)setEndTime:(NSDate *)endTime;

@end
