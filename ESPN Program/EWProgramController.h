//
//  EWProgramController.h
//  ESPN Program
//
//  Created by Tzu-Yi Lin on 12/4/20.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EWESPNRequest.h"

@class EWChannelData;
@class EWESPNRequest;
@class EWPopoverWindowController;

@interface EWProgramController : NSObject  <EWESPNRequestDelegate> {
    IBOutlet NSPopUpButton *dateButton;
    IBOutlet NSPopUpButton *channelButton;
    IBOutlet NSProgressIndicator *progressIndicator;
    IBOutlet NSTableView *tableView;
    IBOutlet NSSearchField *searchField;
    
    EWESPNRequest *request;
    NSArray *channels;
	
	EWChannelData *currentChannel;
    BOOL filtered;
    NSMutableArray *filteredProgram;
    
    EWPopoverWindowController *confirmPopover;
}

- (IBAction)channelChanged:(id)sender;
- (IBAction)dateChanged:(id)sender;
- (IBAction)startSearch:(id)sender;

@end
